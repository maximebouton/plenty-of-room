chronologie du projet

Plenty en Room est initité en 2014 à la suite d'un travail collectif autour des enjeux de captation et de réinterprétation du geste avec la classe de troisième année option design-graphique de lʼESAD Grenoble-Valence encadré par Joël Chevrier.
Le logiciel prends initialement la forme dʼun espace de dessin zoomable.
En 2015 il est développé au game lab du CRI avec lʼaide de Mourdjen Bari, Benjamin Misiak et Alexandre Vaugoux comme un outil de dessin collaboratif, ce travail occasionne un atelier au sein dʼiGamer, un festival du jeu vidéo éducatif et scientifique organisé par le game-lab du CRI. 
Courant 2016 “Plenty of room‟ incorpore un traitement de texte et est utilisé par Annick Lantenois comme un outil de transcription graphique live lors dʼune conférence-performance à La Gaîté Lyrique. Dans le contexte dʼun atelier organisé par les Savanturiers du CRI le logiciel est utilisé dans un contexte dʼexpérimentation de dessin et dʼécriture collective libre.
Avec lʼaide de Vincent Maillard, Denis Pauthier et Ninon Tanga alors étudiants à lʼESAD Grenoble-Valence Plenty of room devient un environnement web collaboratif de dessin, d'écriture et de visualisation de documents axé sur le zoom et la manipulation du point de vue, projet de diplôme de DNA option design graphique de Maxime en 2016.
En 2017 lors dʼun second atelier organisé par les Savanturiers du CRI le logiciel est utilisé dans une version tablette comme environnement de construction et dʼexploration spatial collectif.
