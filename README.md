# Readme

## Commands

lancer node sur le serveur en ssh même après déconnexion

    nohup long-running-process &

    exit

ou sinon voir systemd, pm2 ou screen

get PID

    ps -ef | grep "command name"

kill PID

    kill xxxx

## TODO
 
- identifier items par pseudo
- [?] Id par item
- coordonnées xyz dans l'url
- [X] preview on top
- [X] Dessin libre dans la profondeur
- room par url
- [text ok] shared preview text/svg
+ globally restructure code
  - svg data with main pts not interpreted stroke pts
* langage de live-coding + traitement de texte
  + [X] pseudo(str)
  + [X] color(str)
  + moveTo(pt)
    - synchronisation camera, choisie + random, sync(users[i])
  + drawing
    - strokeColor (dedicated path) sc(str)
    - fillColor (dedicated path) fc(str)
    - strokeWeight (dedicated path) sw(float)
- [X] spatialiser écrans + souris partagés
- [X] organiser le code en ~~modules node~~ scripts séparés
- taille image/media/texte « normalisée »
+ [?] style
  - [X] typo partagée
+ [?] possibilité de grouper les items (par id)
  - [?] export svg text/path par nom de groupe
  - cacher/voir groupes
- [snap] export/envoie des items
- possibilité dʼinterpréter spatialement, temporairement les items (en groupe)
