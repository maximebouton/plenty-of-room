var app = require('express')(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    express = require('express'),
    fs = require('fs'),
    path = require("path"),
    home = "./";

global.fetch = require("node-fetch");

app.use(express.static(home));

app.get('/', function(req, res){
    res.sendFile('index.html', { root: home });
});

function postData(url = ``, data = {}) {
  // Default options are marked with *
    fetch(url, {
        method: "post", // *GET, POST, PUT, DELETE, etc.
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
    .then(res => res.json())
    .then(json => console.log(json))
    .catch((err) => console.log(err));
}

io.on('connection', function(socket){
    console.log('user connected : ' + socket.id);

    fs.readFile(home+'/src/json/data.json', 'utf8', function (err, data) {
        if (err) return console.log(err);
        var objs = JSON.parse(data);
        socket.emit('add', objs);
    });

    // screen + mouse

    socket.extra = {};
    socket.extra.attributes = {};
    socket.extra.attributes.color;
    socket.extra.preview = {};
    socket.extra.preview.text;
    socket.extra.preview.svg;
    socket.extra.mouse = {};
    socket.extra.mouse.x;
    socket.extra.mouse.y;
    socket.extra.mouse.d;
    socket.extra.mouse.c;
    socket.extra.screen = {};
    socket.extra.screen.x;
    socket.extra.screen.y;
    socket.extra.screen.w;
    socket.extra.screen.h;

    socket.emit('id', socket.id);
    socket.emit('sockets', Object.keys(io.sockets.connected));
    for (var i in io.sockets.connected) {
        if (i !== socket.id) {
            var obj = {};
            obj.id = i;
            obj.parameters = io.sockets.connected[i].extra;
            socket.emit('socket', obj);
        }
    }

    socket.broadcast.emit('conn', socket.id);

    socket.on('socket', function(obj) {
        io.sockets.connected[socket.id].extra = obj; // all extra or ?
        var obj = {};
        obj.id = socket.id;
        obj.parameters = io.sockets.connected[i].extra;
        socket.broadcast.emit('socket', obj);
    });

    // on

    socket.on('disconnect', function(){
        console.log('user disconnected : ' + socket.id);
        io.emit('disc', socket.id);
    });
    
    // screenshot
    
    socket.on('screenshot', function(obj){
        obj.date = new Date();
        obj.id = socket.id;
        console.log(obj);

        // save screenshot to node screenshots directory
//         var datetime = new Date().toISOString().replace(/T/, '_').replace(/:/g, '-').replace(/\..+/, '');
//         var base64Image = obj.data.split(';base64,').pop();
//         fs.writeFile(home+'src/screenshots/'+obj.id+'_'+datetime+'.png', base64Image, {encoding: 'base64'}, function(err) {
//             console.log('err');
//         });
// 
        // send screenshot to JEREMIIIE
        //postData(`http://192.168.5.112:3000/methods/documents.plenty.insert`,obj)
        postData(`http://contextes.io/methods/documents.plenty.insert`,obj)
    });

    // receive the new obj, add to data.json and send it to all clients
    socket.on('add', function(obj){
        obj.date = new Date();
        obj.id = socket.id;
        console.log(obj);

        fs.readFile(home+'src/json/data.json', 'utf8', function (err, data) {
            if (err) return console.log(err);
            var objs = JSON.parse(data);
            objs.push(obj);
            fs.writeFile(home+"/src/json/data.json", JSON.stringify(objs, null, 4), function(err) {
                if (err) return console.log(err); 
                console.log("obj added");
                var objs = [];
                objs.push(obj);
                if(!obj.ctx){
                    console.log("postdata")
                    postData(`http://contextes.io/methods/documents.plenty.insert`,obj)
                }
                io.emit('add', objs); // only the new obj but in an array so we handle it with the same update function
            }); 
        });
    });
   
    // receive mouse movement and send it to all clients
    socket.on('move', function(parameters) {
        parameters.id = socket.id;
        socket.broadcast.emit('move', parameters);
    });

});

http.listen(3001, function(){
    console.log('listening on *:3001');
});
