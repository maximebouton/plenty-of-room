// # random
r = {}

// ## intd
r.i = function(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

// ## point
r.p = function(n, min, max) {
	var pts=[]
    for(var i=0;i<n;i++)pts.push([r.i(min,max),r.i(min,max)])
    return pts
}

// ## array
r.a = function(arr) {
    var n = r.i(0,arr.length);
    return arr[n];
}

// # Data
d = {}

// ## Hex
d.h = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']

// ## pre_adjectif
d.pra = ["grand.e","petit.e","super","vaste","modeste","discret.e","luxueux.se","important.e","provocant.e"]

// ## animals
d.a = ["chien.ne","paresseux.se","chat.te","tortue","poule.t","vache","canard","renard","cochon.ne","flamant","éléphant","écureuil","lapin.e","mouton.ne","lion.,e","tigre.sse","ours","kangourou","panda","loutre","araignée","panda","cheval","loup","biche","âne","pingouin.ne","zèbre","pigeon.ne","souris","giraffe","hamster","autruche","otarie","requin","baleine","dauphin"]

// ## post_adjectif
d.psa = ["malin.e","nazi","communiste","capitaliste","hippie","politique","hipster","futé.e","anarchiste","écolo","paresseux.se","prof","simplet.te","grincheux.se","joyeux.se","timide","dormeur.se","financier.ère","docteur.e","reporter","cosmique","mystique","sauvage","joueur.se","magique","sorcier.ère","costaud.e","grognon.e","poète","cuisinier.ère","musicien.ne","bricoleur.se","coquet.te","farceur.euse","maladroit.e","gourmand.e"]
