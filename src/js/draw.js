// # draw

var start=true

var lines=[]
var dc = "black" // drawcolor
var dm = 0 // drawmode

Line = function(){
    this.path = document.createElementNS("http://www.w3.org/2000/svg", 'path')
    this.path.style.stroke = "none"
    this.fill = dc
    this.path.style.fill = this.fill
    this.dm = dm
    this.pts = []
    this.ptsA = []
    this.ptsZ = []
    document.getElementById("svg_preview").appendChild(this.path);
    lines.push(this)

    this.draw()
}

Line.prototype.draw = function() {

    if(usr.move.dir[0] != usr.move.prevdir[0]){
        if(usr.move.dir[0]>usr.move.prevdir[0]){
            if(this.ptsA.length > 0) {
                this.ptsA[this.ptsA.length-1][1]+=10/this.ptsA[this.ptsA.length-1][2]
            }
            offsetX = 5
        } else {
            if(this.ptsA.length > 0) {
                this.ptsA[this.ptsA.length-1][1]-=10/this.ptsA[this.ptsA.length-1][2]
            }
            offsetX = -5
        }
        usr.move.prevdir[0] = usr.move.dir[0]
    }

    if(usr.move.dir[1] != usr.move.prevdir[1]){
        if(usr.move.dir[1]>usr.move.prevdir[1]){
            if(this.ptsA.length > 0) {
                this.ptsA[this.ptsA.length-1][0]+=10/this.ptsA[this.ptsA.length-1][2]
            }
            offsetY = 5
            
        } else {
            if(this.ptsA.length > 0) {
                this.ptsA[this.ptsA.length-1][0]-=10/this.ptsA[this.ptsA.length-1][2]
            }
            offsetY = -5   
        }
        usr.move.prevdir[1] = usr.move.dir[1]
    }

    var xA = usr.x+offsetY
    var xZ = usr.x-offsetY
    var yA = usr.y+offsetX
    var yZ = usr.y-offsetX

    this.pts.push([(cam.cX-(cam.cX-usr.x+cam.x*cam.z)/cam.z),(cam.cY-(cam.cY-usr.y+cam.y*cam.z)/cam.z),cam.z])
    this.ptsA.push([(cam.cX-(cam.cX-xA+cam.x*cam.z)/cam.z),(cam.cY-(cam.cY-yA+cam.y*cam.z)/cam.z),cam.z])
    this.ptsZ.push([(cam.cX-(cam.cX-xZ+cam.x*cam.z)/cam.z),(cam.cY-(cam.cY-yZ+cam.y*cam.z)/cam.z),cam.z])

    if(this.ptsA.length > 1) {
        
        var path= ""
        var pathZ = ""
        var pathA = ""

        var z = this.ptsA.length-1
        var a = 0

        while(z){

            path += " S "+(this.pts[a][0])+","+(this.pts[a][1])
            var x = (this.pts[a][0]+this.pts[a+1][0])/2
            var y = (this.pts[a][1]+this.pts[a+1][1])/2
            path += ","+(x)+","+(y)

            pathA += " S "+(this.ptsA[a][0])+","+(this.ptsA[a][1])
            var x  = (this.ptsA[a][0]+this.ptsA[a+1][0])/2
            var y  = (this.ptsA[a][1]+this.ptsA[a+1][1])/2
            pathA += ","+(x)+","+(y)

            a++

            pathZ += " S "+(this.ptsZ[z][0])+","+(this.ptsZ[z][1])
            var x  = (this.ptsZ[z][0]+this.ptsZ[z-1][0])/2
            var y  = (this.ptsZ[z][1]+this.ptsZ[z-1][1])/2
            pathZ += ","+(x)+","+(y)

            z--

        }

        if(this.dm == 0){
            this.path.setAttribute("d"," M "+this.ptsA[0][0]+' '+this.ptsA[0][1]+pathA+pathZ+' z')
        }else{
            console.log(path)
            this.path.setAttribute("d"," M "+this.pts[0][0]+' '+this.pts[0][1]+path+' z')
        }
    
    }
    if(c.user) c.user.preview.draw(this.path.getAttribute('d'))
}
            
Line.prototype.close = function() {
    document.getElementById("svg_preview").removeChild(this.path)
    var obj = {};
    obj.type = "svg";
    obj.zRef = 1/this.pts[this.pts.length-1][2]
    obj.data = this.path.outerHTML;
    add(obj); 
    if(c.user) c.user.preview.draw('')
}    

