// # Main

document.addEventListener('contextmenu', function(e) {
    e.preventDefault();
}, false);

// ### camera

cam = {
    x:0,
    y:0,
    z:1,
    cX:window.innerWidth/2,
    cY:window.innerHeight/2
}

// ## user

var usr = {
    move : {
        stat:false,
        time:0,
        index:2,
        resolution:2,
        dir:[0,0],
        prevdir:[0,0],
    },
    doc : "default",
    x : 0,
    y : 0,
    prevx : 0,
    prevy : 0,
}

// ## call_cxt
var ctx_x = 0, ctx_y = 0, ctx_z = 1, last_ref = 0, last_ctn = ""

function call_ctx(){
fetch('http://contextes.io/publications/documents', {
  method: 'GET'
})
.then(function(response) { return response.json(); })
.then(function(json) {
    last_doc = {}
    for( var i = 0; i < json.documents.length; i++){
        if(json.documents[i].ref > last_ref) {
            last_ref = json.documents[i].ref
            if(json.documents[i].auteur != "plenty" && json.documents[i].type == "texte") {
                last_doc = json.documents[i]
                console.log(i, json.documents)
            }
        }
    }
    if(last_doc.contenu && last_ctn != last_doc.contenu){

    var item = {}
    item.x = ctx_x
    ctx_x += (-500 + Math.random()*1000)*ctx_z
    item.y = ctx_y
    ctx_y += (-500 + Math.random()*1000)*ctx_z
    item.z = ctx_z
    ctx_z = Math.random() > 0.5 ? ctx_z * 1.25 : ctx_z / 1.25
    item.ctx = true
    item.type = "text"
    item.data = "<p class='ctx'>"+last_doc.contenu+"</p>"
    last_ctn = last_doc.contenu
    console.log(last_doc)
    console.log(ctx_z,item.data)
    add(item)
    }
    setTimeout(function(){call_ctx()},1000)
});
}

// ## draw (refresh dom update)

var time = 0;

function draw(){
    time++;

    if ( time > usr.move.time + 4 ) usr.move.stat = false
    
    if (usr.move.stat) {
        usr.move.index++
        if(usr.move.index > usr.move.resolution) {
            if(!start) lines[lines.length-1].draw()
            usr.move.index = 0
        }
    }

    trigger()
    cam.cX = window.innerWidth/2
    cam.cY = window.innerHeight/2

    for(var i = 0; i < items.length; i ++) items[i].update()
    
    //CRADO ZONE BEGIN
    var svg = document.getElementById("svg")            //CRADO
    // for(var i = 0; i < svg.children.length; i++){    //CRADO
    //     path_update(svg.children[i])                 //CRADO
    // }
    var svgs = document.getElementsByClassName("svgs")
    for(var i =0; i< svgs.length; i++){                 // CRADO
//        if(svgs[i].preview) continue
        svgs[i].style.transform = svg.style.transform   // VERY IMMONDE
        path_update(svgs[i].children[0])                // CRADO
    }                                                   // CRADO

    for ( var i in c.sockets ) c.sockets[i].mouse.update()
    for ( var i in c.sockets ) c.sockets[i].screen.update()
    if(c.user) c.user.mouse.move()
    if(c.user) c.user.screen.move()
    if(c.user){
        document.getElementById("pseudo").textContent = c.user.attributes.parameters.pseudo;
        document.getElementById("pseudo").style.color = c.user.attributes.parameters.color;
        document.getElementById("coor").textContent = 'x:'+cam.x+',y:'+cam.y+',z:'+cam.z
        c.user.updateUrlParameters({'x':cam.x,'y':cam.y,'z':cam.z});

    }
    // CRADO ZONE END
    
    setTimeout(function(){
        draw()
    },25)
}

draw()

// ## editor

editor = document.createElement("textarea")
editor.id = "editor"; document.body.appendChild(editor)
preview = document.createElement("div")
preview.id = "preview"; document.body.appendChild(preview)
preview_content = document.createElement("div")
preview.appendChild(preview_content)

// ## svg

var svg = new Item(0,0,1,'',"svg")
var svg_preview = new Item(0,0,1,'',"svg")
svg.dom = document.getElementById("svg")
svg_preview.dom = document.getElementById("svg_preview")

// ## dom events

window.addEventListener("load",function(e){

    window.addEventListener("keydown",function(e){
        if(!e.ctrlKey || e.key == 'v')editor.focus()
        if(editor.value[0] != '$') preview_content.innerHTML=editor.value
        if(c.user) c.user.preview.write()
        events[e.key] = true
        if(!usr.move.stat) usr.move.stat = true
        usr.move.time = time
    })
    
    window.addEventListener("keyup",function(e){
        if(editor.value[0] != '$') preview_content.innerHTML=editor.value
        if(c.user) c.user.preview.write()
        events[e.key] = false
    })
    
    window.addEventListener("mousedown",function(e){
        events[e.type+e.button] = true
    })
    
    window.addEventListener("mouseup",function(e){
        events[e.type+e.button] = false
    })
    
    window.addEventListener("mousemove",function(e){
        usr.move.stat = true
        usr.move.time = time
        usr.x = e.clientX
        usr.y = e.clientY
        if(e.clientX != usr.prevx) { // used for drawing
            if(e.clientX > usr.prevx) usr.move.dir[0] = 1
            if(e.clientX < usr.prevx) usr.move.dir[0] = -1
            usr.prevx = e.clientX
        }
        if(e.clientY != usr.prevy) { // used for drawing
            if(e.clientY > usr.prevy) usr.move.dir[1] = -1
            if(e.clientY < usr.prevy) usr.move.dir[1] = 1
            usr.prevy = e.clientY
        }
        preview.style.marginLeft = usr.x + "px";
        preview.style.marginTop = usr.y + "px";
    })

    window.addEventListener("resize",c.user.screen.resize(), false);

})

// screenshot (snap)

function snap(str) {
    if(!str)str = "no comment"
    console.log('screenshot');
    var obj = {};
    obj.comment = str;
    obj.type = 'screenshot';
    obj.x = c.user.screen.parameters.x;
    obj.y = c.user.screen.parameters.y;
    obj.z = c.user.screen.parameters.z;

    domtoimage.toPng(document.body)
        .then(function (dataUrl) {
            obj.data = dataUrl;
            socket.emit('screenshot', obj);
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
}
