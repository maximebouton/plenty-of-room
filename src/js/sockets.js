// # Socket

Socket = function(id) {
    this.id = id;
    this.attributes = new Attributes(this);
    this.label = new Label(this);
    this.mouse = new Mouse(this);
    this.screen = new Screen(this);
    this.preview = new Preview(this);
}

Socket.prototype.update = function(parameters) {
    this.attributes.update(parameters.attributes);
    this.mouse.update(parameters.mouse);
    this.screen.update(parameters.screen);
    this.preview.update(parameters.preview);
}

Socket.prototype.emit = function() {
    var obj = {};
    obj.color = this.color
    obj.attributes = this.attributes.parameters;
    obj.mouse = this.mouse.parameters;
    obj.screen = this.screen.parameters;
    obj.preview = this.preview.parameters;
    socket.emit('socket', obj);
}

Socket.prototype.remove = function() {
    this.label.el.parentNode.removeChild(this.label.el);
    this.mouse.el.parentNode.removeChild(this.mouse.el);
    this.screen.el.parentNode.removeChild(this.screen.el);
    // this.preview.svg_el.parentNode.removeChild(this.preview.svg_el);
    delete c.sockets[this.id];
}

Socket.prototype.updateUrlParameters = function(coordinates) {
        var params = '?';
        for (var i in coordinates) {
            params += i+'=';
            params += parseFloat(coordinates[i]);
            params += '&';
        }
        params = params.slice(0, -1);
        var newUrl = this.url.origin += params;
        window.history.pushState("", "", newUrl);
    }

Label = function(socket) {        
    this.socket = socket;
}

Label.prototype.create = function() {
    this.el = document.createElement('a');
    this.el.href = '#' + this.socket.id;
    this.el.appendChild(document.createTextNode(this.socket.id));
    document.getElementById('labels').appendChild(this.el);
}

// ## client
var c = {};
c.user;
c.sockets = {};

// ## node

var socket = io();

// add new object to database
function add(obj) {
    // if(obj.ctx) return
    socket.emit('add', obj);
};

socket.on('add', function(objs) {
    for (var i=0; i<objs.length; i++) {
        // handle objects ici
        if(objs[i].type == "svg"){
            // var svg = document.getElementById("svg")
            var svg = document.createElementNS("http://www.w3.org/2000/svg","svg")
            svg.classList.add("svgs")
            svg.classList.add("item")
            svg.innerHTML += objs[i].data 
            //console.log(objs[i].zRef)
            svg.children[svg.children.length-1].dataset.zRef = objs[i].zRef
            document.body.appendChild(svg)
            // # CRADO, revoir structure objets items, lines, faire une fonction create path à item ? Fonction de spatialisation
        } else {
            new Item(objs[i].x,objs[i].y,objs[i].z,objs[i].data,objs[i].type)
        }
    }
});

// ### id

socket.on('id', function(id) {
    console.log(id);

    c.user = new Socket(id)

    c.user.screen.parameters.x = window.pageXOffset;
    c.user.screen.parameters.y = window.pageYOffset;
    c.user.screen.parameters.w = window.innerWidth;
    c.user.screen.parameters.h = window.innerHeight;

    c.user.url = new URL(window.location.href);

    if (c.user.url.searchParams.get('x')) cam.x = parseFloat(c.user.url.searchParams.get('x'));
    if (c.user.url.searchParams.get('y')) cam.y = parseFloat(c.user.url.searchParams.get('y'));
    if (c.user.url.searchParams.get('z')) cam.z = parseFloat(c.user.url.searchParams.get('z'));
    
    c.user.emit(); 
});

// ### sockets

socket.on('sockets', function(sockets) {
    sockets.splice(sockets.indexOf(c.user.id), 1);
    for (var i = 0; i < sockets.length; i ++) {
        c.sockets[sockets[i]] = new Socket(sockets[i]);
        c.sockets[sockets[i]].label.create();
        c.sockets[sockets[i]].mouse.create();
        c.sockets[sockets[i]].screen.create();
        c.sockets[sockets[i]].preview.create();
    }
});

// ### socket

socket.on('socket', function(obj) {
    c.sockets[obj.id].update(obj.parameters);
});

// ### conn

socket.on('conn', function(id) {
    console.log('connection:' + id);
    c.sockets[id] = new Socket(id);
    c.sockets[id].label.create();
    c.sockets[id].mouse.create();
    c.sockets[id].screen.create();
    c.sockets[id].preview.create();
});

// ### disc

socket.on('disc', function(id) {
    console.log('disconnect:' + id);
    c.sockets[id].remove();
});


