// # Events

events = {}

function trigger(){
    if(events["PageUp"]) cam.z *= 1.025
    if(events["PageDown"]) cam.z /= 1.025
    if(events["ArrowLeft"]) cam.x += 10/cam.z
    if(events["ArrowRight"]) cam.x -= 10/cam.z
    if(events["ArrowUp"]) cam.y += 10/cam.z
    if(events["ArrowDown"]) cam.y -= 10/cam.z/*
    if(events["Enter"]) {
        if(!editor.value.length) return events["mousedown0"] = false
        if(editor.value[0] == '$'){
            toEval = editor.value.slice(1, editor.value.length)
            try{eval(toEval)}catch(e){}
        }
        editor.blur();
    }*/
    if(events["mouseup2"]) c.user.mouse.up()
    if(events["mousedown2"]) {
        c.user.mouse.down()
        if(start){
            line = new Line()
            start=false
        }else{
            lines[lines.length-1].draw()
            lines[lines.length-1].close()
            start=true
        }
        events["mousedown2"] = false
    }
    if(events["mouseup0"]) c.user.mouse.up()
    if(events["mousedown0"]) {
        c.user.mouse.down()
        if(!editor.value.length) return events["mousedown0"] = false
        if(editor.value[0] == '$'){
            toEval = editor.value.slice(1, editor.value.length)
            try{eval(toEval)}catch(e){}
        }else{
            var obj = {};
            obj.x = cam.cX-(cam.cX-usr.x+cam.x*cam.z)/cam.z
            obj.y = cam.cY-(cam.cY-usr.y+cam.y*cam.z)/cam.z
            obj.z = 1/cam.z;
            obj.type = "text";
            obj.data = editor.value;
            obj.doc = usr.doc
            //obj.user = document.getElementById("user").value;
            add(obj); 
            events["mousedown0"] = false
        }
            editor.value = "";
            preview_content.innerHTML = editor.value;
        editor.blur();
    }
}
