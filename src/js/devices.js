// # devices

Preview = function(socket){
    this.socket = socket
    this.parameters = {}
    this.parameters.text
    this.parameters.d
}

Preview.prototype.create = function(){
    this.svg = document.createElementNS("http://www.w3.org/2000/svg","svg")
    this.svg.classList.add("svgs")
    this.path = document.createElementNS("http://www.w3.org/2000/svg","path")
    this.svg.appendChild(this.path)
    document.body.appendChild(this.svg)
}

Preview.prototype.update = function(parameters){
    for (var i in parameters) this.parameters[i] = parameters[i]
    this.render()
}

Preview.prototype.render =  function(){
    this.path.setAttribute('d',this.parameters.d)
//    this.svg.style.transform = "scale("+1*cam.z+")"

    this.parameters.text // 
    ? this.socket.mouse.el.text_preview.innerHTML = this.parameters.text
    : this.socket.mouse.el.text_preview.innerHTML = ''
}

Preview.prototype.draw = function(d) {
    this.parameters.d = d
    this.socket.emit();
}

Preview.prototype.write = function() {
    this.parameters.text = preview_content.innerHTML
    this.socket.emit();
}

// ## attributes

Attributes = function(socket){
    this.socket = socket
    this.parameters = {}
    this.parameters.color = '#'+r.a(d.h)+r.a(d.h)+r.a(d.h)
    this.parameters.pseudo = r.a(d.pra)+'_'+r.a(d.a)+'_'+r.a(d.psa)
}

Attributes.prototype.update = function(parameters) {
    for (var i in parameters) this.parameters[i] = parameters[i]
    this.render()
}

Attributes.prototype.render = function() {
    this.socket.screen.el.style.outline = 'solid 1px '+this.parameters.color
    this.socket.mouse.el.pseudo.textContent = this.parameters.pseudo
    this.socket.mouse.el.style.color = this.parameters.color
    this.socket.mouse.el.mouse.style.backgroundColor = this.parameters.color
}

// ## mouse

Mouse = function(socket) {        
    this.socket = socket;
    this.parameters = {};
    this.parameters.x;
    this.parameters.y;
    this.parameters.z;
    this.parameters.d;
    this.parameters.c;
}

Mouse.prototype.create = function() {
    this.el = document.createElement('div');
    this.el.mouse = document.createElement('div');
    this.el.mouse.classList.add('mouse');
    this.el.appendChild(this.el.mouse)
    this.el.text_preview = document.createElement('div');
    this.el.text_preview.classList.add("text_preview");
    this.el.appendChild(this.el.text_preview);
    this.el.pseudo = document.createElement('p');
    this.el.pseudo.classList.add("pseudo");
    this.el.pseudo.classList.add("mono");
    this.el.appendChild(this.el.pseudo);
    document.body.appendChild(this.el);
}

Mouse.prototype.update = function(parameters) {
    for (var i in parameters) {
        this.parameters[i] = parameters[i];
    }
    this.render()
}

Mouse.prototype.render = function() {
    var x = cam.cX-(cam.cX-this.parameters.x-cam.x)*cam.z
    var y = cam.cY-(cam.cY-this.parameters.y-cam.y)*cam.z
    this.el.style.transform = "translateX("+parseInt(x)+"px) translateY("+parseInt(y)+"px) scale("+this.parameters.z*cam.z+")"
}

Mouse.prototype.down = function() {
    this.parameters.x = usr.x;
    this.parameters.y = usr.y;
    this.parameters.z = cam.z
    this.parameters.d = true;
    this.socket.emit();
}

Mouse.prototype.up = function() {
    this.parameters.x = usr.x;
    this.parameters.y = usr.y;
    this.parameters.z = cam.z
    this.parameters.d = false;
    this.socket.emit();
}

Mouse.prototype.move = function() {
    this.parameters.x = cam.cX-(cam.cX-usr.x+cam.x*cam.z)/cam.z
    this.parameters.y = cam.cY-(cam.cY-usr.y+cam.y*cam.z)/cam.z
    this.parameters.z = 1/cam.z
    this.socket.emit();
}

// ## screen

Screen = function(socket) {
    this.socket = socket;
    this.parameters = {};
    this.parameters.x;
    this.parameters.y;
    this.parameters.z;                
    this.parameters.w;
    this.parameters.h;
    this.parameters.c;
}

Screen.prototype.create = function() {
    this.el = document.createElement('div');
    this.el.classList.add('screen');
    document.body.appendChild(this.el);
}

Screen.prototype.update = function(parameters) {
    for (var i in parameters) {
        this.parameters[i] = parameters[i];
    }
    this.render();
}

Screen.prototype.render = function() {
    var x = cam.cX-(cam.cX-this.parameters.x-cam.x)*cam.z
    var y = cam.cY-(cam.cY-this.parameters.y-cam.y)*cam.z
    this.el.style.width     = this.parameters.w * this.parameters.z * cam.z + 'px';
    this.el.style.height    = this.parameters.h * this.parameters.z * cam.z + 'px';
    this.el.style.transform = "translateX("+parseInt(x)+"px) translateY("+parseInt(y)+"px)"
}

Screen.prototype.move = function() {
    this.parameters.x = cam.cX-(cam.cX-0+cam.x*cam.z)/cam.z
    this.parameters.y = cam.cY-(cam.cY-0+cam.y*cam.z)/cam.z
    this.parameters.z = 1/cam.z
    this.socket.emit();
}

Screen.prototype.resize = function() {
    this.parameters.w = window.innerWidth / cam.z;
    this.parameters.h = window.innerHeight / cam.z;
    this.socket.emit();
}
