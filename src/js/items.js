// # Items

items = []

Item = function(x,y,z,content,type){
    this.dom = document.createElement("div")
    this.dom.classList.add("item")
    this.dom.innerHTML = content
    this.type = type
    this.x = x
    this.y = y
    this.z = z
    this.update()
    items.push(this)
    document.body.appendChild(this.dom)
}

function path_update(path){
        //console.log(path.dataset.zRef)
        if(path.dataset.zRef * cam.z > 1){
            if( 1 + ( 1 - ( path.dataset.zRef * cam.z )/10) < 0.1 ){
                path.style.visibility = "hidden"
                return
            }else{
                if(path.style.visibility != "visible") path.style.visibility = "visible"
            }
            path.style.opacity = 1+(1 - ( path.dataset.zRef * cam.z)/10)
        }else{
            if( (path.dataset.zRef * cam.z)*5 < 0.1 ){
                path.style.visibility = "hidden"
                return
            }else{
                if(path.style.visibility != "visible") path.style.visibility = "visible"
            }
            path.style.opacity = (path.dataset.zRef * cam.z)*5
        }
}

Item.prototype.update = function() {
    if(this.dom.id != "svg" && this.type != "svg"){ // CRADO
        
        if(this.z * cam.z > 1){
            if( 1+(1 - ( this.z * cam.z)/10) < 0.1 ){
                this.dom.style.visibility = "hidden"
                return
            }else{
                if(this.dom.style.visibility != "visible") this.dom.style.visibility = "visible"
            }
            this.dom.style.opacity = 1+(1 - ( this.z * cam.z)/10)
        }else{
            if( this.z * cam.z < 0.1 ){
                this.dom.style.visibility = "hidden"
                return
            }else{
                if(this.dom.style.visibility != "visible") this.dom.style.visibility = "visible"
            }
          this.dom.style.opacity = this.z * cam.z
        }
    }
    x = cam.cX-(cam.cX-this.x-cam.x)*cam.z
    y = cam.cY-(cam.cY-this.y-cam.y)*cam.z
    this.dom.style.transform = "translateX("+x+"px) translateY("+y+"px) scale("+this.z*cam.z+")"
}
