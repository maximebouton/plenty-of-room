#!/usr/bin/env bash

datetime=$(date +"%Y-%m-%d_%H-%M-%S")
echo $datetime.json

cp "src/json/data.json" "src/json/archives/$datetime.json"
echo "[]" > "src/json/data.json"

